# Temporal variability and cell mechanics control robustness in mammalian embryogenesis

## Move permanently
This repository has been moved to [fabreges-2024](https://gitlab.com/fabreges/fabreges-2024)

## Authors
Dimitri Fabrèges<sup>1,#,\*</sup>, Bernat Corominas Murtra<sup>2,\*</sup>, Prachiti Moghe<sup>1,#</sup>,
Alison Kickuth<sup>1</sup>, Takafumi Ichikawa<sup>3,4</sup>, Chizuru Iwatani<sup>5</sup>,
Tomoyuki Tsukiyama<sup>3,5</sup>, Nathalie Daniel<sup>6</sup>, Julie Gering<sup>7</sup>,
Anniek Stokkermans<sup>7</sup>, Adrian Wolny<sup>8</sup>, Anna Kreshuk<sup>8</sup>, Véronique Duranthon<sup>6</sup>,
Virginie Uhlmann<sup>9</sup>, Edouard Hannezo<sup>10,\*</sup>, Takashi Hiiragi<sup>1,3,4,#,\*</sup>

- <sup>1</sup>Developmental Biology Unit, European Molecular Biology Laboratory, 69117, Heidelberg, Germany
- <sup>2</sup>Institute of Biology, University of Graz, 8010 Graz, Austria
- <sup>3</sup>Institute for the Advanced Study of Human Biology (WPI-ASHBi), Kyoto University, Kyoto, 606-8501, Japan
- <sup>4</sup>Department of Developmental Biology, Graduate School of Medicine, Kyoto University, Kyoto, 606-8501, Japan
- <sup>5</sup>Research Center for Animal Life Science, Shiga University of Medical Science, Shiga, 520-2192, Japan
- <sup>6</sup>UVSQ, INRAE, BREED, Université Paris-Saclay, 78350, Jouy-en-Josas, France
- <sup>7</sup>Hubrecht Institute, 3584 CT Utrecht, Netherland
- <sup>8</sup>Cell Biology and Biophysics Unit, European Molecular Biology Laboratory, 69117, Heidelberg, Germany
- <sup>9</sup>EMBL-EBI, Wellcome Genome Campus, Hinxton, CB10 1SD, UK
- <sup>10</sup>Institute of Science and Technology Austria, 3400 Klosterneuburg, Austria
- <sup>#</sup>Current address: Hubrecht Institute, 3584 CT Utrecht, Netherland
- <sup>\*</sup>Correspondence: [d.fabreges@hubrecht.eu](mailto:d.fabreges@hubrecht.eu); [bernat.corominas-murtra@uni-graz.at](mailto:bernat.corominas-murtra@uni-graz.at); [edouard.hannezo@ist.ac.at](mailto:edouard.hannezo@ist.ac.at); [t.hiiragi@hubrecht.eu](mailto:t.hiiragi@hubrecht.eu)

## Abstract
How living systems achieve precision in form and function despite their intrinsic stochasticity is a fundamental yet
open question in biology. Here, we establish a quantitative morphomap of pre-implantation embryogenesis in mouse,
rabbit and monkey embryos, which reveals that although blastomere divisions desynchronise passively without
compensation, 8-cell embryos still display robust 3D structure. Using topological analysis and genetic perturbations in
mouse, we show that embryos progressively change their cellular connectivity to a preferred topology, which can be
predicted by a simple physical model where noise and actomyosin-driven compaction facilitate topological transitions
lowering surface energy. This favours the most compact embryo packing at the 8- and 16-cell stage, thus promoting
higher number of inner cells. Impairing mitotic desynchronisation reduces embryo packing compactness and generates
significantly more cell mis-allocation and a lower proportion of inner-cell-mass-fated cells, suggesting that
stochasticity in division timing contributes to achieving robust patterning and morphogenesis.